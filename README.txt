﻿###IMPORTANT###


Veuillez suivre les procédures dans l'ordre d'appariton.

###1) BASE DE DONNÉES###


Path scripts SQL : ../db/db.sql

Path fichiers BD   : ../db
    
####Veuillez utiliser cette procédure pour créer une base de données vide :####
    
    1 - Ouvrir un invité de commande
    2 - ...> cd "Path  du projet"
    3 - TP2> cd db
    4 - db> sqlite3 db.db
    5 - sqlite> .read db.sql
    
    * - Path DB : ../db.db
    

###2) CONFIGURATION###


# Path fichier configuration: ../config.py


# Configuration du courriel : 

    1 - Email    : changer le 3ème paramètre de la ligne 14
    2 - Password : changer le 3ème paramètre de la ligne 16


# Configuration du chemin d'accés à la BD :

    Pour utiliser la BD de tests, veuillez utiliser le path suivant :
        
        * Path BD tests : ../db_tests.db
        
    Sinon, veuillez utiliser le path suivant pour la BD vide : 
        
        * Path BD vide : ../db.db
    
    
    1 - BD : changer le 3ème paramètre de la ligne 22 par le path de la BD que
             vous voulez utiliser


# Créer le fichier de configuration :
    1 - Ouvrir un invité de commande
    2 - $> cd "Path  du projet"
    3 - TP2> python config.py
    
    * - Path : ../config.ini
    
           ---------- 3) CONFIGURATION ----------

Installer d'abord la version de python : https://www.python.org/downloads/
Installer sqlite3 : https://www.tutorialspoint.com/sqlite/sqlite_installation.htm
Installer Flask : $>pip install Flask 

Exécuter les lignes de commandes suivante afin de d'exécuter l'application  :
```
$>set FLASK_APP=index.py
$>flask run 
                ---------- 4) API ----------


# Pour accéder à l'API permettant d'obtenir la liste de tout les animaux 
# disponibles à l'adoption, veuillez accéder à cette route : 

    * http://127.0.0.1:5000/api/animals



                ---------- Credits -----------
                
Texte chargement du site : http://tobiasahlin.com/moving-letters/#11
Spinner : inspiré de http://simbyone.com/30-css-page-preload-animations/ 
Actualisation immédiate pour input file : https://stackoverflow.com/a/41406599
Force password : inspiré de https://martech.zone/javascript-password-strength/
Géolocalisation : inspiré de https://www.khalidabuhakmeh.com/use-javascript-and-mapquest-to-get-a-users-zipcode




