# TP2/token.py

from itsdangerous import URLSafeTimedSerializer
import io
import configparser

with open("config.ini", "r", encoding="utf-8") as f:
    config_file = f.read()
config = configparser.RawConfigParser(allow_no_value=True)
config.readfp(io.StringIO(config_file))


def generate_confirmation_token(email):
    serializer = URLSafeTimedSerializer(config.get('secret', 'SECRET_KEY'))
    return serializer.dumps(email, salt=config.get('secret',
                                                   'SECRET_PASSWORD_SALT'))


def confirm_token(token, expiration=3600):
    serializer = URLSafeTimedSerializer(config.get('secret', 'SECRET_KEY'))
    try:
        email = serializer.loads(
            token,
            salt=config.get('secret', 'SECRET_PASSWORD_SALT'),
            max_age=expiration
        )
    except Exception:
        return False
    return email
