# TP2/database.py

import sqlite3
import configparser
import io

query_1_word = ("SELECT * FROM animaux WHERE type LIKE ? OR " +
                "race LIKE ? OR province LIKE ? OR ville LIKE ? OR " +
                "code_postal LIKE ? OR date_publication LIKE ?")

query_2_words = ("SELECT * FROM animaux WHERE " +
                 "type LIKE ? AND race LIKE ? OR " +
                 "type LIKE ? AND type LIKE ? OR " +
                 "race LIKE ? AND type LIKE ? OR " +
                 "race LIKE ? AND race LIKE ? OR " +
                 "type LIKE ? AND ville LIKE ? OR " +
                 "ville LIKE ? AND type LIKE ? OR " +
                 "type LIKE ? AND province LIKE ? OR " +
                 "province LIKE ? AND type LIKE ? OR " +
                 "race LIKE ? AND province LIKE ? OR " +
                 "province LIKE ? AND race LIKE ? OR " +
                 "race LIKE ? AND ville LIKE ? OR " +
                 "ville LIKE ? AND race LIKE ? OR " +
                 "code_postal LIKE ? AND type LIKE ? OR " +
                 "code_postal LIKE ? AND race LIKE ? OR " +
                 "race LIKE ? AND code_postal LIKE ? OR " +
                 "type LIKE ? AND code_postal LIKE ? OR " +
                 "date_publication LIKE ? AND type LIKE ? OR " +
                 "date_publication LIKE ? AND race LIKE ? OR " +
                 "date_publication LIKE ? AND ville LIKE ? OR " +
                 "date_publication LIKE ? AND province LIKE ? OR " +
                 "date_publication LIKE ? AND code_postal LIKE ? OR " +
                 "type LIKE ? AND date_publication LIKE ? OR " +
                 "race LIKE ? AND date_publication LIKE ? OR " +
                 "ville LIKE ? AND date_publication LIKE ? OR " +
                 "province LIKE ? AND date_publication LIKE ? OR " +
                 "code_postal LIKE ? AND date_publication LIKE ?")

query_3_words = ("SELECT * FROM animaux WHERE " +
                 "type LIKE ? AND code_postal LIKE ? AND race LIKE ? OR " +
                 "type LIKE ? AND ville LIKE ? AND race LIKE ? OR " +
                 "type LIKE ? AND province LIKE ? AND race LIKE ? OR " +
                 "type LIKE ? AND ville LIKE ? AND date_publication LIKE ? " +
                 " OR type LIKE ? AND province LIKE ? AND date_publication " +
                 "LIKE ? OR type LIKE ? AND code_postal LIKE ? AND " +
                 "date_publication LIKE ? OR " +
                 "type LIKE ? AND date_publication LIKE ? AND race LIKE ? OR "
                 + "type LIKE ? AND race LIKE ? AND province LIKE ? OR " +
                 "type LIKE ? AND race LIKE ? AND code_postal LIKE ? OR " +
                 "type LIKE ? AND race LIKE ? AND ville LIKE ? OR " +
                 "type LIKE ? AND race LIKE ? AND date_publication LIKE ? OR "
                 + "race LIKE ? AND code_postal LIKE ? AND type LIKE ? OR " +
                 "race LIKE ? AND ville LIKE ? AND type LIKE ? OR " +
                 "race LIKE ? AND province LIKE ? AND type LIKE ? OR " +
                 "race LIKE ? AND date_publication LIKE ? AND type LIKE ? OR "
                 + "race LIKE ? AND date_publication LIKE ? AND ville LIKE ?" +
                 " OR race LIKE ? AND date_publication LIKE ? AND province " +
                 "LIKE ? OR race LIKE ? AND date_publication LIKE ? AND " +
                 "code_postal LIKE ? OR race LIKE ? AND type LIKE ? AND " +
                 "province LIKE ? OR " +
                 "race LIKE ? AND type LIKE ? AND code_postal LIKE ? OR " +
                 "race LIKE ? AND type LIKE ? AND ville LIKE ? OR " +
                 "race LIKE ? AND type LIKE ? AND date_publication LIKE ? OR "
                 + "ville LIKE ? AND type LIKE ? AND race LIKE ? OR " +
                 "ville LIKE ? AND race LIKE ? AND type LIKE ? OR " +
                 "province LIKE ? AND type LIKE ? AND race LIKE ? OR " +
                 "province LIKE ? AND race LIKE ? AND type LIKE ? OR " +
                 "code_postal LIKE ? AND type LIKE ? AND race LIKE ? OR " +
                 "code_postal LIKE ? AND race LIKE ? AND type LIKE ? OR " +
                 "date_publication LIKE ? AND type LIKE ? AND race LIKE ? OR "
                 + "date_publication LIKE ? AND race LIKE ? AND type LIKE ? "
                 + "OR type LIKE ? AND race LIKE ? AND race LIKE ? OR " +
                 "ville LIKE ? AND race LIKE ? AND race LIKE ? OR " +
                 "province LIKE ? AND race LIKE ? AND race LIKE ? OR " +
                 "code_postal LIKE ? AND race LIKE ? AND race LIKE ? OR " +
                 "date_publication LIKE ? AND race LIKE ? AND race LIKE ? OR "
                 + "race LIKE ? AND race LIKE ? AND type LIKE ? OR " +
                 "race LIKE ? AND race LIKE ? AND ville LIKE ? OR " +
                 "race LIKE ? AND race LIKE ? AND province LIKE ? OR " +
                 "race LIKE ? AND race LIKE ? AND code_postal LIKE ? OR " +
                 "race LIKE ? AND race LIKE ? AND date_publication LIKE ?")

query_4_words = ("SELECT * FROM animaux WHERE " +
                 "type LIKE ? AND race LIKE ? AND race LIKE ? AND ville " +
                 "LIKE ? OR type LIKE ? AND race LIKE ? AND race LIKE ? " +
                 "AND province LIKE ? OR type LIKE ? AND race LIKE ? AND " +
                 "race LIKE ? AND code_postal LIKE ? OR type LIKE ? AND " +
                 "race LIKE ? AND race LIKE ? AND date_publication LIKE ? " +
                 "OR type LIKE ? AND ville LIKE ? AND race LIKE ? AND race " +
                 "LIKE ? OR type LIKE ? AND province LIKE ? AND race LIKE ? " +
                 "AND race LIKE ? OR type LIKE ? AND code_postal LIKE ? AND " +
                 "race LIKE ? AND race LIKE ? OR type LIKE ? AND " +
                 "date_publication LIKE ? AND race LIKE ? AND race LIKE ? " +
                 "OR ville LIKE ? AND type LIKE ? AND race LIKE ? AND race " +
                 "LIKE ? OR province LIKE ? AND type LIKE ? AND race LIKE ? " +
                 "AND race LIKE ? OR code_postal LIKE ? AND type LIKE ? AND " +
                 "race LIKE ? AND race LIKE ? OR date_publication LIKE ? " +
                 "AND type LIKE ? AND race LIKE ? AND race LIKE ? OR " +
                 "ville LIKE ? AND race LIKE ? AND race LIKE ? AND type " +
                 "LIKE ? OR province LIKE ? AND race LIKE ? AND race LIKE ? " +
                 "AND type LIKE ? OR code_postal LIKE ? AND race LIKE ? AND " +
                 "race LIKE ? AND type LIKE ? OR date_publication LIKE ? " +
                 "AND race LIKE ? AND race LIKE ? AND type LIKE ? OR " +
                 "race LIKE ? AND race LIKE ? AND type LIKE ? AND ville " +
                 "LIKE ? OR race LIKE ? AND race LIKE ? AND type LIKE ? " +
                 "AND province LIKE ? OR race LIKE ? AND race LIKE ? AND " +
                 "type LIKE ? AND code_postal LIKE ? OR race LIKE ? AND " +
                 "race LIKE ? AND type LIKE ? AND date_publication LIKE ? " +
                 "OR race LIKE ? AND race LIKE ? AND ville LIKE ? AND type " +
                 "LIKE ? OR race LIKE ? AND race LIKE ? AND province LIKE ? " +
                 "AND type LIKE ? OR race LIKE ? AND race LIKE ? AND " +
                 "code_postal LIKE ? AND type LIKE ? OR race LIKE ? AND " +
                 "race LIKE ? AND date_publication LIKE ? AND type LIKE ? OR "
                 + "race LIKE ? AND race LIKE ? AND ville LIKE ? AND " +
                 "date_publication LIKE ? OR race LIKE ? AND race LIKE ? " +
                 "AND province LIKE ? AND date_publication LIKE ? OR " +
                 "race LIKE ? AND race LIKE ? AND type LIKE ? AND " +
                 "date_publication LIKE ?")


with open("config.ini", "r", encoding="utf-8") as f:
    config_file = f.read()
config = configparser.RawConfigParser(allow_no_value=True)
config.readfp(io.StringIO(config_file))


def list_animaux(row):
    return {"nom": row[1], "type": row[2], "age": row[3], "race": row[4],
            "description": row[5], "file_name": row[6], "file_content": row[7],
            "id_animal": row[8], "proprio": row[9], "ville": row[10],
            "code_postal": row[11], "province": row[12],
            "date_publication": row[13]}


def list_type(row):
    return {"nb": row[0], "type": row[1]}


def list_race(row):
    return {"nb": row[0], "type": row[1], "race": row[2]}


def list_province(row):
    return {"nb": row[0], "province": row[1]}


def list_ville(row):
    return {"nb": row[0], "province": row[1], "ville": row[2]}


class Database:
    def __init__(self):
        self.connection = None

    def get_connection(self):
        if self.connection is None:
            self.connection = sqlite3.connect(config.get('db', 'PATH'),)
        return self.connection

    def disconnect(self):
        if self.connection is not None:
            self.connection.close()

    def insert_new_member(self, nom, prenom, email, tel, adresse, ville,
                          province, code_postal, salt, hashed_password,
                          id_animal, favoris, confirmed):
        connection = self.get_connection()
        connection.execute(("INSERT INTO users(nom, prenom, email, tel, "
                            "adresse, ville, province, code_postal,salt, "
                            "hash, id_animal, favoris, confirmed) "
                            "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"),
                           (nom, prenom, email, tel, adresse, ville, province,
                            code_postal, salt, hashed_password, None,
                            None, confirmed))

        connection.commit()

    def set_user_confirmed(self, username):
        connection = self.get_connection()

        connection.execute('UPDATE users SET confirmed=? WHERE email=? ',
                           (1, username))

        connection.commit()

    def get_user_login_info(self, username):
        cursor = self.get_connection().cursor()

        cursor.execute(("SELECT salt, hash FROM users WHERE email=?"),
                       (username,))

        user = cursor.fetchone()

        if user is None:
            return None
        else:
            return user[0], user[1]

    def get_user_all_infos(self, username):
        cursor = self.get_connection().cursor()

        cursor.execute(("SELECT * FROM users WHERE email=?"),
                       (username,))

        user = cursor.fetchone()

        if user is None:
            return None
        else:
            return user

    def insert_new_animal(self, nom, typ, age, race, desc, file_name,
                          file_content, id_animal, proprio, ville,
                          code_postal, province, date_publication):
        connection = self.get_connection()
        connection.execute(("INSERT INTO animaux(nom, type, age, race, "
                            "description, file_name, file_content, id_animal, "
                            "proprio, ville, code_postal, province,"
                            "date_publication) "
                            "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"),
                           (nom, typ, age, race, desc, file_name,
                            file_content, id_animal, proprio, ville,
                            code_postal, province, date_publication))
        connection.commit()

    def get_animal_all_infos(self, id_animal):
        cursor = self.get_connection().cursor()

        cursor.execute(("SELECT * FROM animaux WHERE id_animal=?"),
                       (id_animal,))

        animal = cursor.fetchone()

        if animal is None:
            return None
        else:
            return animal

    def get_my_animal_all_infos(self, id_animal):
        cursor = self.get_connection().cursor()

        cursor.execute(("SELECT * FROM animaux WHERE id_animal=?"),
                       (id_animal))

        animal = cursor.fetchone()

        if animal is None:
            return None
        else:
            return animal

    def get_animal_list_all_infos(self, id_animal):
        cursor = self.get_connection().cursor()

        cursor.execute(("SELECT * FROM animaux WHERE id_animal=?"),
                       (id_animal,))

        animal = cursor.fetchone()

        if animal is None:
            return None
        else:
            return list_animaux(animal)

    def update_new_animal_user(self, username, id_animal):
        connection = self.get_connection()

        connection.execute('UPDATE users SET id_animal=? WHERE email=? ',
                           (id_animal, username))

        connection.commit()

    def get_limit_animaux(self, username):
        cursor = self.get_connection().cursor()

        cursor.execute(("SELECT id_animal FROM users WHERE email=?"),
                       (username,))

        nb = cursor.fetchone()

        if nb[0] is None:
            return 0
        else:
            return 1

    def get_id_animal_user(self, username):
        cursor = self.get_connection().cursor()

        cursor.execute(("SELECT id_animal FROM users WHERE email=?"),
                       (username,))

        id_animal = cursor.fetchone()

        if id_animal is None:
            return None
        else:
            return id_animal

    def save_session(self, id_session, username):
        connection = self.get_connection()
        connection.execute(("INSERT INTO sessions(id_session, utilisateur) "
                            "VALUES(?, ?)"), (id_session, username))
        connection.commit()

    def delete_session(self, id_session):
        connection = self.get_connection()
        connection.execute(("DELETE FROM sessions where id_session=?"),
                           (id_session,))
        connection.commit()

    def get_session(self, id_session):
        cursor = self.get_connection().cursor()
        cursor.execute(("SELECT utilisateur FROM sessions WHERE id_session=?"),
                       (id_session,))

        data = cursor.fetchone()

        if data is None:
            return None
        else:
            return data[0]

    def get_img_animal(self, id_animal):
        cursor = self.get_connection().cursor()
        cursor.execute(("SELECT file_content FROM animaux WHERE "
                        "id_animal=?"), (id_animal,))

        img = cursor.fetchone()

        return img[0]

    def get_img_my_animal(self, id_animal):
        cursor = self.get_connection().cursor()
        cursor.execute(("SELECT file_content FROM animaux WHERE "
                        "id_animal=?"), (id_animal))

        img = cursor.fetchone()

        if img is None:
            return None
        else:
            return img[0]

    def get_all_animals(self):
        cursor = self.get_connection().cursor()
        cursor.execute(("SELECT * FROM animaux"))

        animaux = cursor.fetchall()

        if animaux is None:
            return None
        else:
            return [list_animaux(each) for each in animaux]

    def update_infos_animal(self, nom, typ, age, race, desc, name, data,
                            id_animal):
        connection = self.get_connection()

        connection.execute('UPDATE animaux SET nom=?, type=?, age=?, race=?, '
                           'description=?, file_name=?, file_content=? '
                           'WHERE id_animal=?', (nom, typ, age, race, desc,
                                                 name, data, id_animal))
        connection.commit()

    def delete_infos_animal(self, id_animal):
        connection = self.get_connection()
        connection.execute('DELETE FROM animaux WHERE id_animal=?',
                           (id_animal,))
        connection.commit()

    def update_id_animal_user(self, username):
        connection = self.get_connection()
        connection.execute('UPDATE users SET id_animal=NULL WHERE email=?',
                           (username,))
        connection.commit()

    def get_exists_animal(self, id_animal):
        cursor = self.get_connection().cursor()
        cursor.execute(("SELECT * FROM animaux WHERE id_animal=?"),
                       (id_animal))

        exists = cursor.fetchall()

        if exists is None:
            return False
        else:
            return True

    def get_user_exists(self, username):
        cursor = self.get_connection().cursor()
        cursor.execute(("SELECT * FROM users WHERE email=?"),
                       (username,))

        exists = cursor.fetchone()

        if exists is None:
            return None
        else:
            return exists

    def update_infos_user(self, nom, prenom, tel, adresse, ville, province,
                          code_postal, username):
        connection = self.get_connection()
        connection.execute('UPDATE users SET nom=?, prenom=?, tel=?, '
                           'adresse=?, ville=?, province=?, code_postal=? '
                           'WHERE email=?', (nom, prenom, tel, adresse, ville,
                                             province, code_postal, username))
        connection.commit()

    def get_result_search_1_words(self, query):
        cursor = self.get_connection().cursor()
        cursor.execute((query_1_word),
                       (query, query, query, query, query, query))

        results = cursor.fetchall()

        if results is None:
            return None
        else:
            return [list_animaux(each) for each in results]

    def get_result_search_2_words(self, query_1, query_2):
        cursor = self.get_connection().cursor()
        cursor.execute(query_2_words,
                       (query_1, query_2, query_1, query_2, query_1, query_2,
                        query_1, query_2, query_1, query_2, query_1, query_2,
                        query_1, query_2, query_1, query_2, query_1, query_2,
                        query_1, query_2, query_1, query_2, query_1, query_2,
                        query_1, query_2, query_1, query_2, query_1, query_2,
                        query_1, query_2, query_1, query_2, query_1, query_2,
                        query_1, query_2, query_1, query_2, query_1, query_2,
                        query_1, query_2, query_1, query_2, query_1, query_2,
                        query_1, query_2, query_1, query_2))

        results = cursor.fetchall()

        if results is None:
            return None
        else:
            return [list_animaux(each) for each in results]

    def get_result_search_3_words(self, query_1, query_2, query_3):
        cursor = self.get_connection().cursor()
        cursor.execute(query_3_words,
                       (query_1, query_2, query_3, query_1, query_2, query_3,
                        query_1, query_2, query_3, query_1, query_2, query_3,
                        query_1, query_2, query_3, query_1, query_2, query_3,
                        query_1, query_2, query_3, query_1, query_2, query_3,
                        query_1, query_2, query_3, query_1, query_2, query_3,
                        query_1, query_2, query_3, query_1, query_2, query_3,
                        query_1, query_2, query_3, query_1, query_2, query_3,
                        query_1, query_2, query_3, query_1, query_2, query_3,
                        query_1, query_2, query_3, query_1, query_2, query_3,
                        query_1, query_2, query_3, query_1, query_2, query_3,
                        query_1, query_2, query_3, query_1, query_2, query_3,
                        query_1, query_2, query_3, query_1, query_2, query_3,
                        query_1, query_2, query_3, query_1, query_2, query_3,
                        query_1, query_2, query_3, query_1, query_2, query_3,
                        query_1, query_2, query_3, query_1, query_2, query_3,
                        query_1, query_2, query_3, query_1, query_2, query_3,
                        query_1, query_2, query_3, query_1, query_2, query_3,
                        query_1, query_2, query_3, query_1, query_2, query_3,
                        query_1, query_2, query_3, query_1, query_2, query_3,
                        query_1, query_2, query_3, query_1, query_2, query_3))

        results = cursor.fetchall()

        if results is None:
            return None
        else:
            return [list_animaux(each) for each in results]

    def get_result_search_4_words(self, query_1, query_2, query_3, query_4):
        cursor = self.get_connection().cursor()
        cursor.execute(query_4_words,
                       (query_1, query_2, query_3, query_4, query_1, query_2,
                        query_3, query_4, query_1, query_2, query_3, query_4,
                        query_1, query_2, query_3, query_4, query_1, query_2,
                        query_3, query_4, query_1, query_2, query_3, query_4,
                        query_1, query_2, query_3, query_4, query_1, query_2,
                        query_3, query_4, query_1, query_2, query_3, query_4,
                        query_1, query_2, query_3, query_4, query_1, query_2,
                        query_3, query_4, query_1, query_2, query_3, query_4,
                        query_1, query_2, query_3, query_4, query_1, query_2,
                        query_3, query_4, query_1, query_2, query_3, query_4,
                        query_1, query_2, query_3, query_4, query_1, query_2,
                        query_3, query_4, query_1, query_2, query_3, query_4,
                        query_1, query_2, query_3, query_4, query_1, query_2,
                        query_3, query_4, query_1, query_2, query_3, query_4,
                        query_1, query_2, query_3, query_4, query_1, query_2,
                        query_3, query_4, query_1, query_2, query_3, query_4,
                        query_1, query_2, query_3, query_4, query_1, query_2,
                        query_3, query_4, query_1, query_2, query_3, query_4))

        results = cursor.fetchall()

        if results is None:
            return None
        else:
            return [list_animaux(each) for each in results]

    def get_filter_type(self):
        cursor = self.get_connection().cursor()
        cursor.execute("SELECT COUNT(type), type FROM animaux "
                       "GROUP BY type;")

        results = cursor.fetchall()

        if results is None:
            return None
        else:
            return [list_type(each) for each in results]

    def get_filter_race(self):
        cursor = self.get_connection().cursor()
        cursor.execute("SELECT COUNT(type), type, race FROM animaux "
                       "GROUP BY race;")

        results = cursor.fetchall()

        if results is None:
            return None
        else:
            return [list_race(each) for each in results]

    def get_filter_province(self):
        cursor = self.get_connection().cursor()
        cursor.execute("SELECT COUNT(province) ,province FROM animaux "
                       "GROUP BY province")

        results = cursor.fetchall()

        if results is None:
            return None
        else:
            return [list_province(each) for each in results]

    def get_filter_ville(self):
        cursor = self.get_connection().cursor()
        cursor.execute("SELECT COUNT(province), province, ville FROM animaux "
                       "GROUP BY ville;")

        results = cursor.fetchall()

        if results is None:
            return None
        else:
            return [list_ville(each) for each in results]

    def get_favoris(self, username):
        cursor = self.get_connection().cursor()
        cursor.execute(("SELECT favoris FROM users WHERE email=?"),
                       (username,))

        results = cursor.fetchone()

        if results is None:
            return None
        else:
            return results[0]

    def add_favoris(self, username, list_favoris):
        connection = self.get_connection()
        connection.execute('UPDATE users SET favoris=? WHERE email=?',
                           (list_favoris, username))
        connection.commit()

    def get_proprio_infos(self, id_animal):
        cursor = self.get_connection().cursor()
        cursor.execute(("SELECT prenom, email FROM users WHERE id_animal=?"),
                       (id_animal,))

        proprio_email = cursor.fetchone()

        return proprio_email

    def reset_password(self, username, salt, password):
        connection = self.get_connection()
        connection.execute('UPDATE users SET salt=? WHERE email=?',
                           (salt, username))
        connection.execute('UPDATE users SET hash=? WHERE email=?',
                           (password, username))
        connection.commit()

    def mod_password(self, username, salt, password):
        connection = self.get_connection()
        connection.execute('UPDATE users SET salt=? WHERE email=?',
                           (salt, username))
        connection.execute('UPDATE users SET hash=? WHERE email=?',
                           (password, username))
        connection.commit()

    def get_infos_proprio_animal(self, id_animal):
        cursor = self.get_connection().cursor()

        cursor.execute(("SELECT adresse FROM users WHERE id_animal=?"),
                       (id_animal,))

        adresse = cursor.fetchone()

        if adresse is None:
            return None
        else:
            return {"adresse": adresse[0]}
