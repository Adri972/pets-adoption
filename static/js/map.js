//@Credit : https://www.khalidabuhakmeh.com/use-javascript-and-mapquest-to-get-a-users-zipcode

function retrieve_zip() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            
            var callback = "zipcode_callback",
                lat = position.coords.latitude,
                long = position.coords.longitude,
                script = document.createElement("script");

            script.src = "http://open.mapquestapi.com/nominatim/v1/reverse.php?key=IAffAGkIR0Uu8nA3YVQzXTjlhf0ERinn&format=json&json_callback=" + callback + "&lat=" + lat + "&lon=" + long;

            document.getElementsByTagName("head")[0].appendChild(script);
        });
    }
}

function zipcode_callback(json){
    input = document.getElementById("input-geoloc");
    input.setAttribute("value",json['address']["postcode"]);
    $("#input-geoloc").removeClass("loading");

}
