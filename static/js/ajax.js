var list_choices = [];

$(function() {
    $('#send_email_to_proprio').click(function() {
       
        var nom = $('#contact_nom').val(),
            email = $('#contact_email').val(),
            message = $('#contact_message').val(),
            id_animal = $('#send_email_to_proprio').val(),
            copy = $('#send_email_cc').val();
        
        $.ajax({
            url: '/contact_user_animal/' + id_animal,
            data: $('form').serialize(),
            type: 'POST',
            success: function(response) {
                $('#modal_contact_user').modal('hide');
                $('#modal_email_proprio_success').modal('show');
            },
            error: function(error) {
                $('#modal_contact_user').modal('hide');
                $('#modal_email_proprio_error').modal('show');            
            }
        });
    });
});

function addFavoris(id_animal) {
    var heart = document.getElementById("heart_favoris_" + id_animal);
    
    var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                if (xhr.status === 200) {
                    heart.innerHTML = xhr.responseText;
                    heart.removeAttribute("onclick");
                    heart.setAttribute("onclick","deleteFavoris('" + id_animal + "');");
                 } else {
                    window.location.href = "/erreur";
                }
            }
        };
    
    xhr.open("GET", "/add_favoris/" + id_animal, true);
    xhr.send();
    refreshFavoris("add", id_animal);
}

function deleteFavoris(id_animal) {
    var heart = document.getElementById("heart_favoris_" + id_animal);
    
    var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                if (xhr.status === 200) {
                    heart.innerHTML = xhr.responseText;
                    heart.removeAttribute("onclick");
                    heart.setAttribute("onclick","addFavoris('" + id_animal + "');");
                 } else {
                    window.location.href = "/erreur";
                }
            }
        };
    
    xhr.open("GET", "/del_favoris/" + id_animal, true);
    xhr.send();
    refreshFavoris("del", id_animal);
}

function refreshFavoris(event, id_animal) {
    
    if(event === "add"){
        var favoris = document.getElementById('inner_favoris');
        var inner_favoris = document.getElementById('inner_favoris').innerHTML;
    
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                if (xhr.status === 200) {
                    favoris.innerHTML = inner_favoris + xhr.responseText;
                 } else {
                    window.location.href = "/erreur";
                }
            }
        };
    
    xhr.open("GET", "/add_favoris_infos/" + id_animal, true);
    xhr.send();
        
    }else{
        $('#bulle_' + id_animal).remove()
    }    
}


function onClick_pagination(query, nb_page) {
    var div_animaux = document.getElementById("container_animaux");
    
    var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                if (xhr.status === 200) {
                    div_animaux.innerHTML = xhr.responseText;
                    get_length_display_animals();
                 } else {
                    window.location.href = "/erreur";
                }
            }
        };
    
    xhr.open("GET", "/search/" + query + "/filter/" + nb_page, true);
    xhr.send();
}

function onClick_filter(query, id) {
    var div_animaux = document.getElementById("container_animaux");
    set_choice_filter(query, id, query);
    
    var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                if (xhr.status === 200) {
                    div_animaux.innerHTML = xhr.responseText;
                    get_length_display_animals();
                 } else {
                    window.location.href = "/erreur";
                }
            }
        };
    
    xhr.open("GET", "/search/" + get_list_choices() + "/filter/1", true);
    xhr.send();
}

function onClick_filter_geoloc(id) {
    var div_animaux = document.getElementById("container_animaux");
    var code_postal_query = document.getElementById("input-geoloc").value.substring(0,3);
    var code_postal_plain = document.getElementById("input-geoloc").value;
    set_choice_filter(code_postal_query, id, code_postal_plain);
    
    var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                if (xhr.status === 200) {
                    div_animaux.innerHTML = xhr.responseText;
                    get_length_display_animals();
                 } else {
                    window.location.href = "/erreur";
                }
            }
        };
    
    xhr.open("GET", "/search/" + get_list_choices() + "/filter/1", true);
    xhr.send();
}

function onClick_filter_date(query, id) {
    var div_animaux = document.getElementById("container_animaux");
    
    if(query == "today"){
        var date_query = getDate();
        var date_plain = "Aujourd'hui";
    }else if(query == "week"){
        var date_query = getDateWeek();
        var date_plain = "Cette semaine";
        var week = "";
    }else {
        var date_query = getDate().substring(0,7);
        var date_plain = "Ce mois-ci";
    }
    
    set_choice_filter(date_query, id, date_plain);
    
    var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                if (xhr.status === 200) {
                    div_animaux.innerHTML = xhr.responseText;
                    get_length_display_animals();
                 } else {
                    window.location.href = "/erreur";
                }
            }
        };
    
    xhr.open("GET", "/search/" + get_list_choices() + "/filter/1", true);
    xhr.send();
}


function set_choice_filter(query, id, text){
    var filter_choices = document.getElementById("filter_choices");
    
    var new_choice = document.createElement('li');
    
    new_choice.setAttribute("class","under-list-border wbk_button");
    new_choice.setAttribute("id", id);
    new_choice.setAttribute("data-value", query);
            
    new_choice.innerHTML = text + "<button type='button' " +
                            "class='close' aria-label='Close' " +
                                "onclick='reset_choices(\"" + query + "\",\"" 
                            + id + "\");'><span class='cross'>&times;" + 
                            "</span></button>";
    
    if ($("#filter_choices #" + id).length > 0){ 
        var value = $("#" + id).data("value");
        
        var code_postal = new RegExp("^([A-Z])+([0-9])+([A-Z])+$", "g");
        
        if(code_postal.test(value.substring(0,3))){
            value = value.substring(0,3);
        }
            
        var index = list_choices.indexOf(value);

        if (index > -1){
            list_choices.splice(index,1)
        }
        
        var old_choice = document.getElementById(id);
        filter_choices.removeChild(old_choice);
        filter_choices.appendChild(new_choice);
        list_choices.push(query);
    }else{
        filter_choices.appendChild(new_choice); 
        list_choices.push(query);
    }
    
    $('#reset_filter').show();
}

function reset_choices(query, id){
    var filter_choices = document.getElementById("filter_choices");
    var old_choice = document.getElementById(id);
    filter_choices.removeChild(old_choice);
    var index = list_choices.indexOf(query);
    
    if (index > -1){
        list_choices.splice(index,1)
    }
    
    var div_animaux = document.getElementById("container_animaux");

    if(list_choices.length == 0){
        var xhr = new XMLHttpRequest();
            xhr.onreadystatechange = function() {
                if (xhr.readyState === XMLHttpRequest.DONE) {
                    if (xhr.status === 200) {
                        div_animaux.innerHTML = xhr.responseText;
                        get_length_display_animals();
                    } else {
                        window.location.href = "/erreur";
                    }
                }
            };

        xhr.open("GET", "/search/all/filter/1", true);
        xhr.send();
    }else{
        var xhr = new XMLHttpRequest();
            xhr.onreadystatechange = function() {
                if (xhr.readyState === XMLHttpRequest.DONE) {
                    if (xhr.status === 200) {
                        div_animaux.innerHTML = xhr.responseText;
                        get_length_display_animals();
                    } else {
                        window.location.href = "/erreur";
                    }
                }
            };

        xhr.open("GET", "/search/" + get_list_choices() + "/filter/1", true);
        xhr.send();
    }
}   

function get_list_choices(){
    return list_choices.toString().replace(/,/g," ");
}

//http://www.toutjavascript.com/reference/ref-date.getdate.php
function getDate(){
    var date=new Date()
    
    if((date.getMonth() + 1)< 9){
        var month = "0" + (date.getMonth() + 1);
    }else{
        var month = date.getMonth() + 1;
    }
    
    return date.getFullYear() + "-" + month + "-" + date.getDate();
}

function getDateWeek(){
    var date = getDate();
    var day = parseInt(date.substring(8,10));
    var listDayWeek = [];
    
    for(var i = (day-6); i <= day; i++){
        listDayWeek.push(date.substring(0,8) + i);
    }
    
    console.log(listDayWeek);
    return listDayWeek;
} 

$("#reset_filter").click(function(e) {
    var xhr = new XMLHttpRequest();
    var div_animaux = document.getElementById("container_animaux");

    xhr.onreadystatechange = function() {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status === 200) {
                div_animaux.innerHTML = xhr.responseText;
                get_length_display_animals();
            } else {
                console.log('Erreur avec le serveur');
            }
        }
    };

    xhr.open("GET", "/search/all/filter/1", true);
    xhr.send();
    
    removeAllChoices();
    $("#reset_filter").hide(1000);
});

function removeAllChoices(){
    var listChoices = document.getElementById("filter_choices");
    list_choices = []
    
    while (listChoices.firstChild) {
        listChoices.removeChild(listChoices.firstChild);
    }
}
