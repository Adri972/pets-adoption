//@Credit : https://martech.zone/javascript-password-strength/

function passwordChanged() {
    var force = document.getElementById("force");
        
    var veryStrongRegex = new RegExp("^(?=.{13,25})(?=.*[A-Z])(?=.*[a-z])(?=(.*[0-9]){3,})(?=.*\\W).*$", "g"); // chars, digits and specials, length = 13-25
    var strongRegex = new RegExp("^(?=.{10,12})(?=.*[A-Z])(?=.*[a-z])(?=(.*[0-9]){2,}).*$", "g");// chars, digits*2 and specials, length = 10-12
    var mediumRegex = new RegExp("^(?=.{6,25})(?=.*[A-Z])(?=.*[a-z])(?=(.*[0-9]){2,}).*$", "g");// chars, digits*2 and specials, length = 6-25
    var weakRegex = new RegExp("^(?=.{6,25})(?=.*[A-Z])(?=.*[a-z]).*$", "g"); // chars, digits length = 6-25 
    var notEnoughRegex = new RegExp("(?=.{5,}).*", "g");
    
    var pwd = document.getElementById("password");
    
    if (pwd.value === "") {
        force.style.color = "#dc3545;";
        pwd.setAttribute("class", "form-control is-invalid");
        $('#password_strength').hide(500);
    } else if (false == notEnoughRegex.test(pwd.value)) {
        force.style.color = "#dc3545";
        pwd.setAttribute("class", "form-control is-invalid");
        $('#password_strength').show(500);
        $('#force_text').text("😒");
        $('#password_strength').css("transition",".1s");
    } else if (veryStrongRegex.test(pwd.value)) {
        force.style.color = "purple";
        pwd.setAttribute("class", "form-control is-valid-very-strong");
        $('#password_strength').show(500);
        $('#force_text').text("😍");
        $('#password_strength').css("transition",".1s");
    } else if (strongRegex.test(pwd.value)) {
        force.style.color = "#28a745";
        pwd.setAttribute("class", "form-control is-valid-strong");
        $('#password_strength').show(500);
        $('#force_text').text("😏");
        $('#password_strength').css("transition",".1s");
    } else if (mediumRegex.test(pwd.value)) {
        force.style.color = "#ffc107";
        pwd.setAttribute("class", "form-control is-valid-medium");
        $('#password_strength').show(500);
        $('#force_text').text("😕");
        $('#password_strength').css("transition",".1s");
    } else if (false == weakRegex.test(pwd.value)) {
        force.style.color = "#dc3545";
        pwd.setAttribute("class", "form-control is-invalid");
        $('#password_strength').show(500);
        $('#force_text').text("😒");
        $('#password_strength').css("transition",".1s");
    } else {
        force.style.color = "#dc3545";
        pwd.setAttribute("class", "form-control is-invalid");
        $('#password_strength').show(500);
        $('#force_text').text("😒");
        $('#password_strength').css("transition",".1s");
    }
}

