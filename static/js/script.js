$(document).ready(function (){
    $('[data-toggle="tooltip"]').tooltip();
    $('.phone_us').mask('(000) 000-0000', {placeholder: "(000) 000-0000"});
    $('.postal_code_ca').mask('S0S 0S0', {placeholder: "X0X 0X0"});
    get_length_display_animals();
    $(".container_spawn").hide();
    $('#password_strength').hide();
    $('#reset_filter').hide();
});

$(window).on('load',function(){
    $('#modal_user_exists').modal('show');
});

function select_option(value) {
    $("#select_province").val(value);
}

function show_login() {
    $('.dropdown-menu').toggleClass("show");
}

$(".img-logo").click(function(){
    window.location.href = "/";
});

/*
Changement auto de la photo de l'animal
Inspiré de https://stackoverflow.com/a/41406599
*/
$("#modifier").click(function(e) {
    $("#inputImgAnimal").click();
});

function fasterPreview( uploader ) {
    if (uploader.files && uploader.files[0] ){
        $('#animal_image').attr('src', window.URL.createObjectURL(uploader.files[0]) );
    }
}

$("#inputImgAnimal").change(function(){
    fasterPreview( this );
});
// ---- end ----//
function rotate_arrow(id){
    var angle = ($("#" + id).data('angle') + 180) || 180;
    $("#" + id).css({'transform': 'rotate(' + angle + 'deg)'});
    $("#" + id).data('angle', angle);
}

function collapse_categ(id_collapse, id_arrow){
    var elem = document.getElementById(id_collapse),
        arrow = document.getElementById(id_arrow);

    collapse_all_categ();
    rotate_arrow(id_arrow);
    reset_filter_radio();
}

function collapse_all_categ(){
    $(".collapse_categ").collapse("hide");
    $(".arrow_categ").css("transform", "rotate(0deg)");
}

function collapse_sous_categ_race(id_collapse, id_arrow){
    var elem = document.getElementById(id_collapse);
    var arrow = document.getElementById(id_arrow);
    
    collapse_all_sous_categ_race();
    rotate_arrow(id_arrow);
}

function collapse_all_sous_categ_race(){
    $(".collapse_race").collapse("hide");
    $(".arrow_race").css("transform", "rotate(0deg)");
}

function collapse_categ_lieux(id_collapse, id_arrow){
    var elem = document.getElementById(id_collapse);
    var arrow = document.getElementById(id_arrow);
    
    collapse_all_categ_lieux();
    rotate_arrow(id_arrow);
}

function collapse_all_categ_lieux(){
    $(".collapse_lieux").collapse("hide");
    $(".arrow_lieux").css("transform", "rotate(0deg)");
}

function collapse_sous_categ_lieux(id_collapse, id_arrow){
    var elem = document.getElementById(id_collapse);
    var arrow = document.getElementById(id_arrow);
    
    collapse_all_sous_categ_lieux();
    rotate_arrow(id_arrow);
}

function collapse_all_sous_categ_lieux(){
    $(".collapse_province").collapse("hide");
    $(".arrow_province").css("transform", "rotate(0deg)");
}

function reset_filter_radio(){
    $('#filter :radio').prop('checked',false);
}

function show_filter(){

    if ($("#toggle_favoris").css("width") == "90px" || $("#toggle_geoloc").css("width") == "90px"){
        $("#filter").css("left","0");
        $("#filter").css("transition",".3s ease");
        $("#toggle_filter").removeAttr("onclick");
        $("#toggle_filter").attr("onclick", "hide_filter()");
        $("#toggle_filter").css("width", "90px");
        $("#toggle_filter").css("left", "15%");
        $("#toggle_filter").css("transition", ".3s ease");
        
        $("#favoris").css("left","-15%");
        $("#favoris").css("transition",".3s ease");
        $("#toggle_favoris").removeAttr("onclick");
        $("#toggle_favoris").attr("onclick", "show_favoris()");
        $("#toggle_favoris").css("width", "64px");
        $("#toggle_favoris").css("left", "15%");
        $("#toggle_favoris").css("transition", ".3s ease");

        $("#geoloc").css("left","-15%");
        $("#geoloc").css("transition",".3s ease");
        $("#toggle_geoloc").removeAttr("onclick");
        $("#toggle_geoloc").attr("onclick", "show_geoloc()");
        $("#toggle_geoloc").css("width", "64px");
        $("#toggle_geoloc").css("left", "15%");
        $("#toggle_geoloc").css("transition", ".3s ease");
    }else{      
        $("#filter").css("left","0");
        $("#filter").css("transition",".3s ease");
        $("#toggle_filter").removeAttr("onclick");
        $("#toggle_filter").attr("onclick", "hide_filter()");
        $("#toggle_filter").css("width", "90px");
        $("#toggle_filter").css("transition", ".3s ease");
        $("#toggle_filter").css("left", "15%");
        $("#toggle_filter").css("transition",".3s ease");
        $("#toggle_favoris").css("left", "15%");
        $("#toggle_favoris").css("transition",".3s ease");
        $("#toggle_geoloc").css("left", "15%");
        $("#toggle_geoloc").css("transition",".3s ease");
    }
}

function hide_filter(){
    $("#filter").css("left","-15%");
    $("#filter").css("transition",".3s ease");
    $("#toggle_filter").removeAttr("onclick");
    $("#toggle_filter").attr("onclick", "show_filter()");
    $("#toggle_filter").css("width", "64px");
    $("#toggle_filter").css("transition", ".3s ease");
    $("#toggle_filter").css("left", "0");
    $("#toggle_filter").css("transition",".3s ease");
    $("#toggle_favoris").css("left", "0");
    $("#toggle_favoris").css("transition",".3s ease");
    $("#toggle_geoloc").css("left", "0");
    $("#toggle_geoloc").css("transition",".3s ease");
}

function show_favoris(){
    
    if ($("#toggle_filter").css("width") == "90px" || $("#toggle_geoloc").css("width") == "90px"){
        $("#favoris").css("left","0");
        $("#favoris").css("transition",".3s ease");
        $("#filter").css("left","-15%");
        $("#filter").css("transition",".3s ease");
        $("#geoloc").css("left","-15%");
        $("#geoloc").css("transition",".3s ease");
        
        $("#toggle_favoris").removeAttr("onclick");
        $("#toggle_favoris").attr("onclick", "hide_favoris()");
        $("#toggle_favoris").css("width", "90px");
        $("#toggle_favoris").css("left", "15%");
        $("#toggle_favoris").css("transition", ".3s ease");
        
        $("#toggle_filter").removeAttr("onclick");
        $("#toggle_filter").attr("onclick", "show_filter()");
        $("#toggle_filter").css("width", "64px");
        
        $("#toggle_geoloc").removeAttr("onclick");
        $("#toggle_geoloc").attr("onclick", "show_geoloc()");
        $("#toggle_geoloc").css("width", "64px");
        $("#toggle_geoloc").css("left", "15%  ");
        

    }else{      
        $("#favoris").css("left","0");
        $("#favoris").css("transition",".3s ease");
        $("#toggle_favoris").removeAttr("onclick");
        $("#toggle_favoris").attr("onclick", "hide_favoris()");
        $("#toggle_favoris").css("width", "90px");
        $("#toggle_favoris").css("transition", ".3s ease");
        $("#toggle_favoris").css("left", "15%");
        $("#toggle_favoris").css("transition",".3s ease");
        $("#toggle_filter").css("left", "15%");
        $("#toggle_filter").css("transition",".3s ease");
        $("#toggle_geoloc").css("left", "15%");
        $("#toggle_geoloc").css("transition",".3s ease");
    }
}

function hide_favoris(){
    $("#favoris").css("left","-15%");
    $("#filter").css("left","-15%");
    $("#geoloc").css("left","-15%");
    $("#favoris").css("transition",".3s ease");
    $("#toggle_favoris").removeAttr("onclick");
    $("#toggle_favoris").attr("onclick", "show_favoris()");
    $("#toggle_favoris").css("width", "64px");
    $("#toggle_favoris").css("transition", ".3s ease");
    $("#toggle_favoris").css("left", "0");    
    $("#toggle_favoris").css("transition",".3s ease");
    $("#toggle_filter").css("left", "0");    
    $("#toggle_filter").css("transition",".3s ease");
    $("#toggle_geoloc").css("left", "0");    
    $("#toggle_geoloc").css("transition",".3s ease");
}


function user_geoloc(){
    $("#input-geoloc").toggleClass("loading");
    retrieve_zip(); 
}


function get_length_display_animals(){
    var length = $('.body_list').length;
    var margin_top = 0.0;
    
    if (length == 1){
       margin_top = 25.5;  
    } else if (length == 2){
        margin_top = 19.45
    } else if (length == 3){
        margin_top = 13.4
    } else if (length == 4){
        margin_top = 7.35
    } else if (length == 5){
        margin_top = 1.3
    }
    
    $("footer").css("margin-top", margin_top + "%")
}

$(window).scroll(function () {
    var scrollTop = $(window).scrollTop();
    if (scrollTop == 0) {
        maxHeight = 903;
    } else{
        maxHeight = 903 - scrollTop;
    }
    
    $('#filter').css("max-height", maxHeight + "px");
    $('#filter').css("transition", ".0001s");
    $('#favoris').css("max-height", maxHeight + "px");
    $('#favoris').css("transition", ".0001s");
    $('#geoloc').css("max-height", maxHeight + "px");
    $('#geoloc').css("transition", ".0001s");

})

function goBack() {
    window.history.back();
}

function show_loader(){
    $("#form_contact").hide();
    $("#loader").toggleClass("loader");
}

function reset_form(){
    $("#form_contact")[0].reset();
    $("#form_contact").show();
    $("#loader").removeAttr("class");
}

$(function(){
    $("#check_copy").click(function(){
        var value = $("#send_email_cc").val();       
        
        if (value == "unchecked"){
            $("#send_email_cc").val("checked");
        } else{
            $("#send_email_cc").val("unchecked");
        }
    });
});