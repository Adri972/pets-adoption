create table users (
  id integer primary key,
  nom varchar(25),
  prenom varchar(25),
  email varchar(100),
  tel varchar(10),
  adresse varchar(100),
  ville varchar(25),
  province varchar(50),
  code_postal varchar(7),
  salt varchar(32),
  hash varchar(128),
  id_animal varchar(50),
  favoris varchar(200),
  confirmed integer(1)
);

create table sessions (
  id integer primary key,
  id_session varchar(32),
  utilisateur varchar(25)
);

create table animaux (
  id integer primary key,
  nom varchar(25),    
  type varchar(25),
  age integer,
  race varchar(25),
  description varchar(500),
  file_name varchar(100),
  file_content blob,
  id_animal varchar(50),
  proprio varchar(50),
  ville varchar(50),
  code_postal varchar(50),
  province varchar(50),  
  date_publication varchar(10)
);