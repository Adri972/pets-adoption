# TP2/index.py


# ------ IMPORTS -----

from flask import Flask
from flask import render_template
from flask import g
from flask import request
from flask import redirect
from flask import session
from flask import url_for
from flask import jsonify
from functools import wraps
from base64 import b64encode
from .database import Database
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from TP2.token import generate_confirmation_token, confirm_token
import hashlib
import uuid
import random
import string
import math
import re
import smtplib
import io
import configparser
import datetime
import urllib

app = Flask(__name__)
app.secret_key = "798e745599d5090d41ed8ef7a89b9c64"

msg_1 = ("Seul les membres inscrit peuvent consulter cette page ! Veuilez "
         + "vous authentifier s'il vous plait !")
error_1 = "Tous les champs sont obligatoires !"
error_2 = "Identifiant ou mot de passe incorect !"
error_3 = "Vous n'avez aucune annonce en ligne actuellement !"
error_4 = ("Un utilisateur avec ce courriel existe déja !\n Veuillez vérifier "
           + "vos informations !")
error_5 = ("Le mot de passe doit contenir les spécificitées suivantes :\n" +
           "    - Au moins 1 lettre majuscule\n" +
           "    - Au moins 1 lettre minuscule\n" +
           "    - Au moins 2 chiffres\n" +
           "    - Au moins 6 caractères\n")
img_file_default = ("https://image.noelshack.com/fichiers/2018/17/2/15245907" +
                    "30-img-default.jpg")

# @Description   : Permet de lire le fichier de configuration du projet
config = "config.ini"
with open(config, "r", encoding="utf-8") as f:
    config_file = f.read()
config = configparser.RawConfigParser(allow_no_value=True)
config.readfp(io.StringIO(config_file))


# @Description   :Permet d'accéder à la base de données du projet
def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        g._database = Database()
    return g._database


# @Description   : Ferme l'accés à la base de données lorsque l'application
#                | est fermée
@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.disconnect()


# ----- WRAPS / ERROR HANDLER-----

# @Situation   : Permet de définir un accés restreint aux utlisateurs
#              | authentifiés sur l'application
def authentication_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if not is_authenticated(session):
            return send_unauthorized()
        return f(*args, **kwargs)
    return decorated


# @Situation   : Permet de définir un accés restreint aux utlisateurs ayant
#              | confirmés leurs comptes sur l'application
def check_confirmed(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if "id" in session:
            username = get_db().get_session(session["id"])
            user = get_db().get_user_all_infos(username)

            if user[13] == 0:
                return redirect('/unconfirmed')

            return f(*args, **kwargs)

    return decorated_function


# @Situation   : Erreur 404 O^_^O
# @Credit      : http://flask.pocoo.org/docs/0.12/patterns/errorpages/
@app.errorhandler(404)
def page_not_found(e):

    if "id" in session:
        username = get_db().get_session(session["id"])
        infos_user = get_db().get_user_all_infos(username)
        limit = get_db().get_limit_animaux(username)

        return render_template('404.html', infos_user=infos_user, login=True,
                               limit=limit), 404

    else:
        return render_template('404.html', login=False), 404


# ----- ROUTES -----

# @Situation   : Page d'accueil de l'application
@app.route('/', methods=['GET', 'POST'])
def home():
    animals = get_db().get_all_animals()

    if animals:
        random_list = random_animals(animals)
        list_animals = decode_file_content_list(random_list)
    else:
        list_animals = None

    if "id" in session:
        username = get_db().get_session(session["id"])
        infos_user = get_db().get_user_all_infos(username)
        limit = get_db().get_limit_animaux(username)

        return render_template('index.html', login=True,
                               infos_user=infos_user, limit=limit,
                               list_animals=list_animals)

    else:
        return render_template('index.html', login=False,
                               list_animals=list_animals)


# @Situation   : Page contenant les animaux disponibles à l'adoption
# @Param       : Le numéro de la page
@app.route('/animaux/<nb_page>', methods=['GET', 'POST'])
def animaux(nb_page):
    if re.match(r"^[0-9]+$", nb_page) is None:
        return check_nb_page(nb_page)

    animals = get_db().get_all_animals()
    list_filter = get_all_filter()
    nb_pages = get_nb_pages_result(animals)
    list_page = get_list_partition_animals(animals, nb_page)

    if not animals:
        return no_db_exception()

    if "id" in session:
        username = get_db().get_session(session["id"])
        infos_user = get_db().get_user_all_infos(username)
        limit = get_db().get_limit_animaux(username)
        list_favoris = get_db().get_favoris(username)
        list_info_favoris = []

        if int(nb_page) < 1 or int(nb_page) > nb_pages:
            return render_template('404.html', infos_user=infos_user,
                                   limit=limit, login=True), 404

        if list_favoris is not None:
            list_favoris = get_db().get_favoris(username).split()
            list_info_favoris = get_infos_favoris(list_favoris)

        list_animals = decode_file_content_list(list_page)

        return render_template('animaux.html', list_animals=list_animals,
                               nb_pages=nb_pages, actual=int(nb_page),
                               infos_user=infos_user, login=True,
                               filter_type=list_filter[0], limit=limit,
                               filter_race=list_filter[1],
                               filter_province=list_filter[2],
                               filter_ville=list_filter[3],
                               list_favoris=list_info_favoris)
    else:
        if int(nb_page) < 1 or int(nb_page) > nb_pages:
            return render_template('404.html', login=False), 404

        list_animals = decode_file_content_list(list_page)

        return render_template('animaux.html', list_animals=list_animals,
                               nb_pages=nb_pages, actual=int(nb_page),
                               login=False, filter_type=list_filter[0],
                               filter_race=list_filter[1],
                               filter_province=list_filter[2],
                               filter_ville=list_filter[3])


# @Situation   : Page d'informations pour un animal
# @Param       : Un id unique pour chaque animal
@app.route('/animaux/fiche/<id_animal>', methods=['GET', 'POST'])
def fiche_animal(id_animal):

    if "id" in session:
        username = get_db().get_session(session["id"])
        infos_user = get_db().get_user_all_infos(username)
        limit = get_db().get_limit_animaux(username)
        list_favoris = get_db().get_favoris(username)
        infos_animal = get_db().get_animal_all_infos(id_animal)

        if infos_animal:
            img_animal = get_db().get_img_animal(id_animal)
            image = b64encode(img_animal).decode('UTF-8')
            est_favoris = False
            list_info_favoris = []

            if list_favoris is not None:
                list_favoris = get_db().get_favoris(username).split()
                est_favoris = id_animal in list_favoris
                list_info_favoris = get_infos_favoris(list_favoris)

            return render_template('fiche.html', infos_animal=infos_animal,
                                   img=image, login=True, limit=limit,
                                   infos_user=infos_user,
                                   list_favoris=list_info_favoris,
                                   est_favoris=est_favoris)
        else:
            return render_template('404.html', infos_user=infos_user,
                                   login=True, limit=limit), 404

    else:
        infos_animal = get_db().get_animal_all_infos(id_animal)

        if infos_animal:
            img_animal = get_db().get_img_animal(id_animal)
            image = b64encode(img_animal).decode('UTF-8')

            return render_template('fiche.html', infos_animal=infos_animal,
                                   img=image, login=False,
                                   infos_user=None)
        else:
            return render_template('404.html', infos_user=None,
                                   login=False), 404


# @Situation   : Page pour une nouvelle inscription sur l'application
@app.route('/new_member_button', methods=['GET', 'POST'])
def new_member():
    return render_template('inscription_user.html', login=False)


# @Situation   : Inscription d'un nouveau membre
# @Return      : Inscription et envoi d'un email de confirmation
@app.route('/new_member_submit', methods=['GET', 'POST'])
def new_member_inscription():
    nom = request.form['nom']
    prenom = request.form['prenom']
    tel = request.form['telephone']
    adresse = request.form['adresse']
    ville = request.form['ville']
    province = request.form['province']
    code_postal = request.form['code_postal'].upper()
    username = request.form['email']
    password = request.form['password']
    id_animal = ""
    favoris = ""
    confirmed = False

    if (nom == "" or prenom == "" or tel == "" or username == ""
        or password == "" or adresse == "" or ville == "" or province == ""
            or code_postal == ""):

        return render_template("inscription_user.html", login=False,
                               errors=error_1.split('/n'))

    if check_minimum_password_strength(password) is None:
        return render_template("inscription_user.html", login=False,
                               errors=error_5.split('\n'))

    if not get_db().get_user_exists(username):
        salt = uuid.uuid4().hex
        hashed_password = (hashlib.sha512(str(password + salt)
                                          .encode("utf-8")).hexdigest())

        get_db().insert_new_member(nom, prenom, username, tel, adresse, ville,
                                   province, code_postal, salt,
                                   hashed_password, id_animal, favoris,
                                   confirmed)

#        token = generate_confirmation_token(username)
#        confirm_url = url_for('confirm_email', token=token, _external=True)
#        html = render_template('activate.html', confirm_url=confirm_url)
#        subject = "Veuillez confirmer votre email !"
#        send_email(config.get('email', 'USERNAME'), username, subject, html,
#                   None)

        return redirect('/confirmation/new_member')
    else:
        return render_template('/confirmation/new_member', login=False,
                               errors=error_4.split('/n'),
                               exist=get_db().get_user_exists(username))


# @Situation   : Page pour l'inscription d'un nouvel animal
# @Wraps       : Authentification requise, Confirmation requise
@app.route('/add_new_animal', methods=['GET', 'POST'])
@authentication_required
@check_confirmed
def new_animal_inscription():

    if "id" in session:
        username = get_db().get_session(session["id"])
        infos_user = get_db().get_user_all_infos(username)
        limit = get_db().get_limit_animaux(username)

        return render_template("inscription_animal.html", login=True,
                               infos_user=infos_user, limit=limit)
    else:
        return send_unauthorized()


# @Situation   : Inscription d'un nouvel animal
# @Return      : Inscription et envoi d'un email de confirmation
@app.route('/new_animal_submit', methods=['POST'])
def new_animal_submit():

    if "id" in session:
        username = get_db().get_session(session["id"])
        infos_user = get_db().get_user_all_infos(username)

        try:
            file = request.files['imgAnimal']
            data = file.read()
            name = file.filename
        except Exception:
            with urllib.request.urlopen(img_file_default) as url:
                file = io.BytesIO(url.read())
                data = file.read()
                name = img_file_default

        nom = request.form['nom']
        typ = request.form['type']
        race = request.form['race']
        age = request.form['age']
        desc = request.form['desc']
        id_animal = id_generator()

        proprio = infos_user[1] + " " + infos_user[2]
        ville = infos_user[6]
        province = infos_user[7]
        code_postal = infos_user[8]
        date_publication = get_date()

        get_db().insert_new_animal(nom, typ, age, race, desc, name, data,
                                   id_animal, proprio, ville, code_postal,
                                   province, date_publication)
        get_db().update_new_animal_user(username, id_animal)

        confirm_url = url_for('fiche_animal', id_animal=id_animal,
                              _external=True)
        html = render_template('new_add.html', confirm_url=confirm_url)
        subject = "Votre annonce est en ligne !"
        send_email(config.get('email', 'USERNAME'), username, subject, html,
                   None)

        return redirect("/confirmation/add_animal")
    else:
        return send_unauthorized()


# @Situation   : Page affichant des confirmations
# @Param       : Le type de la confirmation à afficher
@app.route('/confirmation/<type_of_confirmation>', methods=['GET', 'POST'])
def confirmation(type_of_confirmation):

    if "id" in session:
        username = get_db().get_session(session["id"])
        infos_user = get_db().get_user_all_infos(username)
        limit = get_db().get_limit_animaux(username)

        return render_template("confirmation.html", infos_user=infos_user,
                               login=True, limit=limit,
                               type_conf=type_of_confirmation)
    else:
        return render_template("confirmation.html", login=False,
                               type_conf=type_of_confirmation)


# @Situation   : Page contenant les annonces de l'utilisateur
# @Wraps       : Authentification requise, Confirmation requise
@app.route('/mes_annonces', methods=['GET', 'POST'])
@authentication_required
@check_confirmed
def mes_annonces():

    if "id" in session:
        username = get_db().get_session(session["id"])
        id_animal = get_db().get_id_animal_user(username)
        infos_user = get_db().get_user_all_infos(username)
        limit = get_db().get_limit_animaux(username)

        if id_animal[0] is not None or id_animal[0] == "":
            animal = get_db().get_exists_animal(id_animal)
            img_file = get_db().get_img_my_animal(id_animal)
            infos_animal = get_db().get_my_animal_all_infos(id_animal)
            image = b64encode(img_file).decode('UTF-8')

            return render_template("mon_annonce.html", infos_user=infos_user,
                                   login=True, img=image, exists=animal,
                                   infos_animal=infos_animal, limit=limit)
        else:
            return render_template("mon_annonce.html", infos_user=infos_user,
                                   login=True, exists=False, limit=limit,
                                   error=error_3)
    else:
        return send_unauthorized()


# @Situation   : Page avec les favoris de l'utilisateur
# @Wraps       : Authentification requise , Confirmation requise
@app.route('/mes_favoris', methods=['GET', 'POST'])
@authentication_required
@check_confirmed
def mes_favoris():

    if "id" in session:
        username = get_db().get_session(session["id"])
        infos_user = get_db().get_user_all_infos(username)
        limit = get_db().get_limit_animaux(username)

        list_favoris = get_db().get_favoris(username)
        list_info_favoris = []

        if list_favoris is not None:
            list_favoris = get_db().get_favoris(username).split()

            for favoris in list_favoris:
                infos = get_db().get_animal_list_all_infos(favoris)
                img_file = get_db().get_img_animal(infos['id_animal'])
                infos['file_content'] = b64encode(img_file).decode('UTF-8')
                list_info_favoris.append(infos)

        return render_template("mes_favoris.html", infos_user=infos_user,
                               login=True, limit=limit,
                               list_favoris=list_info_favoris)
    else:
        return send_unauthorized()


# @Situation   : Page pour les informations personnelles de l'utilisateur
# @Wraps       : Authentification requise , Confirmation requise
@app.route('/mes_infos', methods=['GET', 'POST'])
@authentication_required
@check_confirmed
def mod_user():

    if "id" in session:
        username = get_db().get_session(session["id"])
        infos_user = get_db().get_user_all_infos(username)
        limit = get_db().get_limit_animaux(username)

        return render_template("mes_infos.html", infos_user=infos_user,
                               login=True, limit=limit)
    else:
        return send_unauthorized()


# @Situation   : Soumission des modification des informations personelles
@app.route('/mod_user', methods=['POST'])
def mes_infos_user():

    if "id" in session:
        username = get_db().get_session(session["id"])

        nom = request.form['nom']
        prenom = request.form['prenom']
        tel = request.form['tel']
        adresse = request.form['adresse']
        ville = request.form['ville']
        province = request.form['province']
        code_postal = (request.form['code_postal'].upper()).replace(" ", "")

        get_db().update_infos_user(nom, prenom, tel, adresse, ville,
                                   province, code_postal, username)

        return redirect("/confirmation/mod_user")
    else:
        return send_unauthorized()


# @Situation   : Modification d'une annonce d'un animal
# @Param       : Un id unique pour chaque animal
@app.route('/mod_animal/<id_animal>', methods=['POST'])
def mod_annonce(id_animal):
    infos_animal = get_db().get_animal_all_infos(id_animal)

    try:
        file = request.files['imgAnimal']
        data = file.read()
        name = file.filename
    except Exception:
        name = infos_animal[6]
        data = infos_animal[7]

    nom = request.form['nom']
    typ = request.form['type']
    race = request.form['race']
    age = request.form['age']
    desc = request.form['desc']

    get_db().update_infos_animal(nom, typ, age, race, desc, name, data,
                                 id_animal)

    return redirect("/confirmation/mod_animal")


# @Situation   : Suppression d'une annonce de l'utilisateur
# @Param       : Un id unique pour chaque animal
@app.route('/del_animal/<id_animal>', methods=['POST'])
def del_annonce(id_animal):

    if "id" in session:
        username = get_db().get_session(session["id"])
        get_db().delete_infos_animal(id_animal)
        get_db().update_id_animal_user(username)

        return redirect("/mes_annonces")
    else:
        return send_unauthorized()


# @Situation   : Utilisation du champ 'rechercher'
# @Param       : Le type de la recherche désiré ("search" our "filter")
@app.route('/search/<type_of_query>', methods=['GET', 'POST'])
def search(type_of_query):
    query = request.form['query']
    return redirect("/search/" + query + "/" + type_of_query + "/1")


# @Situation   : Recherche / filtre d'animaux
# @Param       : La requête, le type de la requête, le numéro de la page
@app.route('/search/<query>/<type_of_query>/<nb_page>',
           methods=['GET', 'POST'])
def search_query(query, type_of_query, nb_page):
    result_search = check_query(query)
    list_result = result_search[0]
    str_query = result_search[1]
    list_animals = check_list_result(list_result, nb_page)
    nb_pages = get_nb_pages_result(list_result)
    list_filter = get_all_filter()

    if query == "all":
        return get_all_animals(nb_page)

    if "id" in session:
        username = get_db().get_session(session["id"])
        infos_user = get_db().get_user_all_infos(username)
        limit = get_db().get_limit_animaux(username)
        list_favoris = get_db().get_favoris(username)

        if list_favoris is not None:
            list_favoris = list_favoris.split()
            list_info_favoris = get_infos_favoris(list_favoris)

        if type_of_query == "search":
            return render_template('search.html', filter_type=list_filter[0],
                                   list_result=list_animals, nb_pages=nb_pages,
                                   actual=int(nb_page), infos_user=infos_user,
                                   filter_race=list_filter[1], query=str_query,
                                   filter_province=list_filter[2], login=True,
                                   filter_ville=list_filter[3], limit=limit,
                                   list_favoris=list_info_favoris)
        else:
            return render_template("filter.html", query=str_query, limit=limit,
                                   infos_user=infos_user, actual=int(nb_page),
                                   list_animals=list_animals, login=True,
                                   list_favoris=list_info_favoris,
                                   nb_pages=nb_pages)
    else:

        if type_of_query == "search":
            return render_template('search.html', list_result=list_animals,
                                   actual=int(nb_page), nb_pages=nb_pages,
                                   filter_type=list_filter[0], query=str_query,
                                   filter_race=list_filter[1], login=False,
                                   filter_province=list_filter[2],
                                   filter_ville=list_filter[3])
        else:
            return render_template("filter.html", login=False, query=str_query,
                                   list_animals=list_animals,
                                   nb_pages=nb_pages, actual=int(nb_page))


# @Situation   : Ajout d'un favoris et mise à jour de la liste
# @Param       : Un id unique pour chaque animal
# @Wraps       : Authentification requise , Confirmation requise
# @Return      : L'icone 'coeur plein' pour un favoris
@app.route('/add_favoris/<id_animal>', methods=['GET'])
@authentication_required
@check_confirmed
def add_favoris(id_animal):

    username = None

    if "id" in session:
        username = get_db().get_session(session["id"])
        list_favoris = get_db().get_favoris(username)

        if list_favoris is None:
            new_list_favoris = id_animal
        else:
            new_list_favoris = list_favoris + " " + id_animal

        new_list_favoris = re.sub(' +', ' ', new_list_favoris)
        get_db().add_favoris(username, new_list_favoris)

        return ('<i class="fa fa-heart faa-tada animated-hover faa-fast">' +
                '</i>', 200)
    else:
        return send_unauthorized()


# @Situation   : Suppression d'un favoris et mise à jour de la liste
# @Param       : Un id unique pour chaque animal
# @Return      : L'icone 'coeur vide' pour un non favoris
@app.route('/del_favoris/<id_animal>', methods=['GET'])
def del_favoris(id_animal):

    username = None

    if "id" in session:
        username = get_db().get_session(session["id"])
        list_favoris = get_db().get_favoris(username)

        new_list_favoris = list_favoris.replace(id_animal, "")
        new_list_favoris = re.sub(' +', ' ', new_list_favoris)

        get_db().add_favoris(username, new_list_favoris)

        return ('<i class="far fa-heart faa-tada animated-hover faa-fast">' +
                '</i>', 200)
    else:
        return send_unauthorized()


# @Situation   : Ajout du nouveau favoris dans le panneau latéral 'favoris'
# @Param       : Un id unique pour chaque animal
@app.route('/add_favoris_infos/<id_animal>', methods=['GET'])
def refresh_favoris(id_animal):

    infos = get_db().get_animal_list_all_infos(id_animal)
    img_file = get_db().get_img_animal(infos['id_animal'])
    infos['file_content'] = b64encode(img_file).decode('UTF-8')

    return render_template('new_favoris.html', infos=infos)


# @Situation   : Connection de l'utilisateur
# @Return      : Création de la clé de session 'id'
@app.route('/login', methods=['GET', 'POST'])
def log_user():

    username = request.form["email"]
    password = request.form["password"]

    if username == "" or password == "":
        return render_template("error_login.html", login=False, error=error_1)
    else:
        user = get_db().get_user_login_info(username)
        if user is None:
            return render_template("error_login.html", login=False)

        salt = user[0]

        hashed_password = (hashlib.sha512(str(password + salt).encode("utf-8"))
                           .hexdigest())

        if hashed_password == user[1]:
            id_session = uuid.uuid4().hex
            get_db().save_session(id_session, username)
            session["id"] = id_session

            return redirect("/mes_infos")

        else:
            return render_template("error_login.html", login=False)


# @Situation   : Déconnection de l'utilisateur
# @Wraps       : Authentification requise !
# @Return      : Suppression de la clé de session 'id'
@app.route('/logout', methods=['POST'])
@authentication_required
def logout():
    id_session = session["id"]
    session.pop('id', None)
    get_db().delete_session(id_session)

    return redirect("/")


# @Situation   : L'utilisateur n'a pas confirmé son compte
# @Wraps       : Authentification requise !
# @Return      : Demande d'authentification
@app.route('/unconfirmed')
@authentication_required
def unconfirmed():
    username = None

    if "id" in session:
        username = get_db().get_session(session["id"])
        infos_user = get_db().get_user_all_infos(username)

        if infos_user[13] == 1:
            return redirect('/')

        return render_template('unconfirmed.html', infos_user=infos_user,
                               login=True)


# @Situation   : Page de contactez nous
@app.route("/contact_us", methods=['GET'])
def contact_us():

    username = None

    if "id" in session:
        username = get_db().get_session(session["id"])
        infos_user = get_db().get_user_all_infos(username)
        limit = get_db().get_limit_animaux(username)

        return render_template("contact_us.html", login=True, limit=limit,
                               infos_user=infos_user)
    else:
        return render_template("contact_us.html", login=False)


# @Situation   : L'utilisateur envoi un message à notre équipe
# @Return      : Envoi du courriel de contact
@app.route("/contact", methods=['POST'])
def contact_us_submit():

    username = request.form["email"]
    nom = request.form["nom"]
    message = request.form["message"]

    html = render_template('contact_us_email.html', nom=nom, message=message,
                           email=username)
    subject = "Nouveau message de " + nom
    send_email(config.get('email', 'USERNAME'),
               config.get('email', 'USERNAME'), subject, html, None)

    return redirect("confirmation/email_send")


# @Situation   : Page à propos
@app.route("/about_us", methods=['GET'])
def about_us():

    username = None

    if "id" in session:
        username = get_db().get_session(session["id"])
        infos_user = get_db().get_user_all_infos(username)
        limit = get_db().get_limit_animaux(username)

        return render_template("about_us.html", login=True, limit=limit,
                               infos_user=infos_user)
    else:
        return render_template("about_us.html", login=False)


# @Situation   : Page du soutien
@app.route("/help", methods=['GET'])
def help():

    username = None

    if "id" in session:
        username = get_db().get_session(session["id"])
        infos_user = get_db().get_user_all_infos(username)
        limit = get_db().get_limit_animaux(username)

        return render_template("help.html", login=True, infos_user=infos_user,
                               limit=limit)
    else:
        return render_template("help.html", login=False)


# @Situation   : Page des conditions d'utilisation
@app.route("/conditions", methods=['GET'])
def conditions():

    username = None

    if "id" in session:
        username = get_db().get_session(session["id"])
        infos_user = get_db().get_user_all_infos(username)
        limit = get_db().get_limit_animaux(username)

        return render_template("conditions.html", login=True, limit=limit,
                               infos_user=infos_user)
    else:
        return render_template("conditions.html", login=False)


# @Situation   : Page des partenaires
@app.route("/partenaires", methods=['GET'])
def partenaires():

    username = None

    if "id" in session:
        username = get_db().get_session(session["id"])
        infos_user = get_db().get_user_all_infos(username)
        limit = get_db().get_limit_animaux(username)

        return render_template("partenaires.html", login=True,  limit=limit,
                               infos_user=infos_user)
    else:
        return render_template("partenaires.html", login=False)


# @Situation   : L'utilisateur envoi un message au propriétaire d'une anonce
# @Param       : Un id unique pour chaque animal
# @Return      : Envoi d'un courriel avec le message
@app.route("/contact_user_animal/<id_animal>", methods=['POST'])
def contact_user_for_animal(id_animal):

    nom_user = request.form["nom"]
    email_user = request.form["email_contact"]
    message_user = request.form["message"]

    try:
        send_copy_email = request.form["send_email_cc"]

        if send_copy_email == "checked":
            copie = email_user

    except Exception:
        copie = None

    proprio = get_db().get_proprio_infos(id_animal)

    sujet = "Réponse à votre annonce"
    html = render_template('contact_proprio.html', proprio=proprio[0],
                           nom=nom_user, message=message_user,
                           email=email_user, id_animal=id_animal)

    send_email(config.get('email', 'USERNAME'), proprio[1], sujet, html, copie)

    return redirect("/animaux/fiche/" + id_animal)


# @Situation   : L'utilisateur a confirmé son compte avec succés
# @Param       : Le token de confirmation
@app.route('/courriel_confirmation/<token>')
def confirm_email(token):
    try:
        username = confirm_token(token)
        infos_user = get_db().get_user_all_infos(username)

        if infos_user[13] == 1:
            return redirect("/confirmation/email_already_confirmed")
        else:
            get_db().set_user_confirmed(username)

            return redirect("/confirmation/email_confirmed")
    except Exception:
        return render_template("confirmation.html", type_conf="email_expired",
                               infos_user=infos_user)


# @Situation   : L'utilisateur souhaite un renvoi de courriel de confirmation
# @Param       : L'email de l'utlisateur
# @Return      : Renvoi de l'email de comfirmation de compte
@app.route("/send_new_confirmation/<username>", methods=['POST'])
def new_confirmation(username):
    token = generate_confirmation_token(username)
    confirm_url = url_for('confirm_email', token=token, _external=True)
    html = render_template('activate.html', confirm_url=confirm_url)
    subject = "Veuillez confirmer votre email !"
    send_email(config.get('email', 'USERNAME'), username, subject, html, None)

    return redirect('confirmation/new_email')


# @Situation   : Le token envoyé à l'utilisateur est expiré
# @Return      : Renvoi de l'email de confirmation de compte
@app.route("/send_new_confirmation_expired", methods=['POST'])
def new_confirmation_expired():
    username = request.form["email"]
    token = generate_confirmation_token(username)
    confirm_url = url_for('confirm_email', token=token, _external=True)
    html = render_template('activate.html', confirm_url=confirm_url)
    subject = "Veuillez confirmer votre email !"
    send_email(config.get('email', 'USERNAME'), username, subject, html, None)

    return redirect('confirmation/new_email')


# @Situation   : Mot de passe oublié
# @Return      : Page pour entrer son courriel
@app.route("/reset_password", methods=['GET', 'POST'])
def password_reset():
    return render_template("reset_password.html")


@app.route("/reset_password/<token>", methods=['GET', 'POST'])
def password_reset_confirm(token):
    return render_template("inscription_user.html", reset=True)


# @Situation   : L'utilisateur demande une réinitialisation par email
# @Return      : Confirmation de l'envoi du courriel
@app.route("/reset_password_submit", methods=['POST'])
def password_reset_submit():
    username = request.form["email"]
    infos_user = get_db().get_user_all_infos(username)
    token = generate_confirmation_token(username)
    confirm_url = url_for('password_reset_confirm', token=token,
                          _external=True)
    html = render_template('reset_password_email.html', infos_user=infos_user,
                           confirm_url=confirm_url)
    subject = "Réinitialisation du mot de passe !"
    send_email(config.get('email', 'USERNAME'), username, subject, html, None)

    return redirect("confirmation/reset_password")


# @Situation   : L'utilisateur soumet la requête de réinitialisation de l'email
# @Return      : Confirmation de la réinitialisation
@app.route("/reset_password_change", methods=['GET', 'POST'])
def password_reset_change():
    username = request.form["email"]
    password = request.form["password"]
    salt = uuid.uuid4().hex
    hashed_password = (hashlib.sha512(str(password + salt)
                                      .encode("utf-8")).hexdigest())

    get_db().reset_password(username, salt, hashed_password)

    infos_user = get_db().get_user_all_infos(username)
    limit = get_db().get_limit_animaux(username)

    return render_template("confirmation.html", login=False,
                           infos_user=infos_user, limit=limit,
                           type_conf="reset_password_confirm")


# @Situation   : L'utilisateur accède à l'outil pour modifier son mot de passe
# @Return      : La page permettant à l'utilisateur de faire ses modifications
@app.route("/mod_password", methods=['GET', 'POST'])
@authentication_required
def mod_password():

    if "id" in session:
        username = get_db().get_session(session["id"])
        infos_user = get_db().get_user_all_infos(username)
        limit = get_db().get_limit_animaux(username)

        return render_template("mod_password.html", login=True, limit=limit,
                               infos_user=infos_user)


# @Situation   : L'utilisateur soumet son nouveau mot de passe au système
# @Wraps       : Authentification requise !
# @Return      : Confirmation de la modification
@app.route("/mod_password_submit", methods=['POST'])
@authentication_required
def mod_password_submit():
    username = request.form["email"]
    password = request.form["password"]
    salt = uuid.uuid4().hex
    hashed_password = (hashlib.sha512(str(password + salt)
                                      .encode("utf-8")).hexdigest())

    if check_minimum_password_strength(password) is None:
        return render_template("mod_password.html", login=False,
                               errors=error_5.split('\n'))

    get_db().mod_password(username, salt, hashed_password)
    infos_user = get_db().get_user_all_infos(username)
    html = render_template('mod_password_email.html', infos_user=infos_user)
    subject = "Modification de votre mot de passe !"
    send_email(config.get('email', 'USERNAME'), username, subject, html, None)

    return redirect("confirmation/mod_password")


# @Description : API pour obtenir la liste des animaux au format JSON
# @Return      : la liste des animaux en format JSON
@app.route('/api/animals', methods=["GET"])
def liste_pays():

    list_animals = get_db().get_all_animals()
    list_animals_final = []

    for animal in list_animals:
        infos_user = get_db().get_infos_proprio_animal(animal['id_animal'])
        del animal['file_name']
        del animal['file_content']
        all_infos = {**animal, **infos_user}
        list_animals_final.append(all_infos)

    return jsonify(list_animals_final)


# @Situation   : Message d'erreur en cas d'erreur côté serveur
@app.route('/erreur', methods=["GET", "POST"])
def server_error():

    if "id" in session:
        username = get_db().get_session(session["id"])
        infos_user = get_db().get_user_all_infos(username)
        limit = get_db().get_limit_animaux(username)

        return render_template("server_error.html", login=True, limit=limit,
                               infos_user=infos_user)
    else:
        return render_template("server_error.html", login=False)


# ----- FONCTIONS UTILES -----

# @Situation   : Erreur dans le numéro de page (< 0 ou non [0-9])
# @Param       : Le numéro à vérifier
def check_nb_page(nb_page):
    if "id" in session:
        username = get_db().get_session(session["id"])
        infos_user = get_db().get_user_all_infos(username)
        limit = get_db().get_limit_animaux(username)

        return render_template('404.html', infos_user=infos_user,
                               limit=limit, login=True), 404
    else:
        return render_template('404.html', login=False), 404


# @Description : Vérifier si l'utilisateur est connecté
# @Param       : Session du navigateur
# @Return      : la valeur de la clé 'id' dans les sessions => courriel
def is_authenticated(session):
    return "id" in session


# @Situation   : Utilisateur non authentifié
def send_unauthorized():
    return (render_template('inscription_user.html', msg=msg_1, login=False),
            401)


# @Description : Générateur d'un id unique pour un animal parmis 10^36
#              | combinaision possible
# @Param       : la taille de l'id voulu, liste des caractéres à combiner
# @Return      : l'id unique pour un animal
# @Credit      :https://stackoverflow.com/a/2257449
def id_generator(size=10, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


# @Description : Générer une liste de 5 animaux aléatoire pour l'acceuil
# @Param       : la liste de tout les animaux de la plateforme
# @Return      : une liste de 5 animaux aléatoire
def random_animals(list_animals):
    list_indice = list(range(0, len(list_animals)))
    list_random_indice = []
    list_random_animals = []

    if len(list_animals) == 0:
        if "id" in session:
                username = get_db().get_session(session["id"])
                infos_user = get_db().get_user_all_infos(username)
                limit = get_db().get_limit_animaux(username)

                return render_template('index.html', login=True,
                                       infos_user=infos_user, limit=limit,
                                       list_animals=None)

        else:
            return render_template('index.html', login=False,
                                   list_animals=list_animals)
    elif len(list_animals) < 5:
        list_random_animals = list_animals
    else:
        list_random_indice = random.sample(list_indice, 5)

        for j in range(len(list_animals)):
            for k in range(len(list_random_indice)):
                if j == list_random_indice[k]:
                    list_random_animals.append(list_animals[j])

    return list_random_animals


# @Description : Décoder le contenu du champ 'file_content' dans
#              | l'enregistrement de l'utilisateur dans la base de donnée sur
#              | la liste passée en paramètre
# @Param       : la liste à décoder
def decode_file_content_list(liste):
    for item in liste:
        temp = b64encode(item['file_content']).decode('UTF-8')
        item['file_content'] = temp

    return liste


# @Description : Obtenir la liste de tout les animaux en cas de
#              | réinitialisation du filtre
# @Param       : le numéro de la pagee_not_found
# @Return      : la liste de tout les animaux
def get_all_animals(nb_page):
    animals = get_db().get_all_animals()
    list_page = partition_list_animaux(animals)[int(nb_page)-1]
    nb_pages = get_nb_pages_result(animals)
    list_animals = decode_file_content_list(list_page)

    if "id" in session:
        username = get_db().get_session(session["id"])
        list_favoris = get_db().get_favoris(username)
        list_info_favoris = []

        if list_favoris is not None:
            list_favoris = get_db().get_favoris(username).split()
            list_info_favoris = get_infos_favoris(list_favoris)

        return render_template("all_animals.html", list_animals=list_animals,
                               nb_pages=nb_pages, actual=int(nb_page),
                               list_favoris=list_info_favoris)
    else:
        return render_template("all_animals.html", list_animals=list_animals,
                               nb_pages=nb_pages, actual=int(nb_page))


# @Description : Obtenir le nombre de pages à afficher
# @Param       : la liste des animaux pour déterminer le nombre de pages
# @Return      : le nombre de pages de résultats
def get_nb_pages_result(list_animals):

    if len(list_animals) % 5 == 0:
        nb_pages = math.floor(len(list_animals)/5)
    else:
        nb_pages = math.floor(len(list_animals)/5)+1

    return nb_pages


# @Description : Obtenir la liste des animaux fragmentée en sous listes
#              | de 5 éléments chacunes
# @Param       : la liste des animaux, le numéro de page
# @Return      : la sous liste correspondant au numéro de page
def get_list_partition_animals(animals, nb_page):

    if "id" in session:
        username = get_db().get_session(session["id"])
        infos_user = get_db().get_user_all_infos(username)
        limit = get_db().get_limit_animaux(username)

        try:
            list_page = partition_list_animaux(animals)[int(nb_page)-1]
            return list_page

        except IndexError:
            return render_template('404.html', infos_user=infos_user,
                                   limit=limit, login=True), 404

    else:
        try:
            list_page = partition_list_animaux(animals)[int(nb_page)-1]
            return list_page

        except IndexError:
            return render_template('404.html', login=False), 404


# @Situation : Aucun animals dans la base de donnée
def no_db_exception():
    if "id" in session:
        username = get_db().get_session(session["id"])
        infos_user = get_db().get_user_all_infos(username)
        limit = get_db().get_limit_animaux(username)

        return render_template('animaux.html', login=True, limit=limit,
                               infos_user=infos_user)
    else:
        return render_template('animaux.html', login=False)


# @Description : Partionner la liste des animaux en paramètre pour un affichage
#              | de 5 animaux par pages
# @Param       : la liste des animaux à partionner
# @Return      : la liste des animaux partionnée
def partition_list_animaux(list_animals):
    list_of_partitions = []

    for i in range(0, len(list_animals), 5):
        list_of_partitions.append(list_animals[i:i+5])

    return list_of_partitions


# @Description : Récupérer la date du jour sur le système
# @Return      : la date du jour au format YYYY-MM-DD
def get_date():
    jour = datetime.date.today()
    type(jour)

    isoDate = datetime.date(jour.year, jour.month, jour.day).isoformat()

    return str(isoDate)


# @Description : Diriger la requête de la recherche soumise par l'utilisateur
#              | en fonction de sa taille
# @Param       : la requête de l'utilisateur
# @Return      : la liste des résultats correspondant, la requête nettoyée à
#              | afficher à l'utilisateur
def check_query(query):
    query = "%" + query + "%"
    full_query = query.split()
    list_result = []
    str_query = ""
    taille = len(full_query)

    if taille == 1:
        query_1 = full_query[0]
        list_result = get_db().get_result_search_1_words(query_1)
        str_query = query_1

    elif taille == 2:
        query_1 = full_query[0] + "%"
        query_2 = "%" + full_query[1]
        str_query = (query_1 + " " + query_2)
        list_result = get_db().get_result_search_2_words(query_1, query_2)

    elif taille == 3:
        query_1 = full_query[0] + "%"
        query_2 = "%" + full_query[1] + "%"
        query_3 = "%" + full_query[2]
        str_query = (query_1 + " " + query_2 + " " + query_3)
        list_result = get_db().get_result_search_3_words(query_1, query_2,
                                                         query_3)
    elif taille == 4:
        query_1 = full_query[0] + "%"
        query_2 = "%" + full_query[1] + "%"
        query_3 = "%" + full_query[2] + "%"
        query_4 = "%" + full_query[3]
        str_query = (query_1 + " " + query_2 + " " + query_3 + " " + query_4)
        list_result = get_db().get_result_search_4_words(query_1, query_2,
                                                         query_3, query_4)

    str_query = str_query.replace("%", "")

    return (list_result, str_query)


# @Description : Paramétrer et envoyer des courriels
# @Param       : la source, les destinataire, le sujet, le message et le
#              | destinataire en copie
def send_email(source, destination, sujet, message, copie):
    source_address = source
    body = message
    subject = sujet

    msg = MIMEMultipart()
    msg['Subject'] = subject
    msg['From'] = source_address
    msg['To'] = destination

    if copie is not None:
        msg['Cc'] = copie
        destination_address = [destination] + [copie]
    else:
        destination_address = [destination]

    msg.attach(MIMEText(body, 'html'))

    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.starttls()
    server.login(source_address, config.get('email', 'PASSWORD'))
    text = msg.as_string()
    server.sendmail(source_address, destination_address, text)
    server.quit()


# @Description : Récupérer les infos des 'id_animal' dans la liste des favoris
#              | de l'utilisateur
# @Param       : la liste des 'id' dans les favoris de l'user
# @Return      : une nouvelle liste complète et décodée des favoris
def get_infos_favoris(list_favoris):
    list_infos_favoris = []

    for favoris in list_favoris:
        infos = get_db().get_animal_list_all_infos(favoris)
        img_file = get_db().get_img_animal(infos['id_animal'])
        infos['file_content'] = b64encode(img_file).decode('UTF-8')
        list_infos_favoris.append(infos)

    return list_infos_favoris


# @Description : Récuperer une liste de tout les filtres
def get_all_filter():
    list_filter = []

    list_filter.append(get_db().get_filter_type())
    list_filter.append(get_db().get_filter_race())
    list_filter.append(get_db().get_filter_province())
    list_filter.append(get_db().get_filter_ville())

    return list_filter


# @Description : Partionner / Decoder la liste des animaux
# @Param       : liste des animaux brute, le numéro de la page actuelle
# @Return      : la liste des animaux partionnée et décodée
def check_list_result(list_result, nb_page):

    if len(list_result) == 0:
        list_animals = list_result
    else:
        list_page = partition_list_animaux(list_result)[int(nb_page)-1]
        list_animals = decode_file_content_list(list_page)

    return list_animals


# @Description : Vérifier si le password soumis par l'user satisfait les
#              | critères de sécurité minimum
# @Param       : la password soumis au système
# @Return      : true si le pssword réussi le test
def check_minimum_password_strength(password):
    minimum = (r"^(?=.{6,25})(?=.*[A-Z])(?=.*[a-z])(?=(.*[0-9]){2,}).*$")

    return re.match(minimum, password)
