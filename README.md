﻿# PETS ADOPTION - PLATEFORME D'ADOPTION D'ANIMAUX  ###


### CONFIGURATION ###

Installer d'abord la version de python : https://www.python.org/downloads/
Installer sqlite3 : https://www.tutorialspoint.com/sqlite/sqlite_installation.htm
Installer Flask : ```$> pip install Flask```

### EXÉCUTION ###

Exécuter les lignes de commandes suivante afin de d'exécuter l'application  :
```
$> set FLASK_APP=index.py
$> flask run 
```

Allez à : http://127.0.0.1:5000

### API ###

Pour accéder à l'API permettant d'obtenir la liste de tout les animaux 
disponibles à l'adoption, veuillez accéder à cette route :

    http://127.0.0.1:5000/api/animals


### Credits ###
                
- Texte chargement du site : http://tobiasahlin.com/moving-letters/#11
- Spinner : inspiré de http://simbyone.com/30-css-page-preload-animations/ 
- Actualisation immédiate pour input file : https://stackoverflow.com/a/41406599
- Force password : inspiré de https://martech.zone/javascript-password-strength/
- Géolocalisation : inspiré de https://www.khalidabuhakmeh.com/use-javascript-and-mapquest-to-get-a-users-zipcode
