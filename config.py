# TP2/config.py

import os
import configparser

file_name = "config.ini"

if not os.path.isfile(file_name):
    config_file = open(file_name, 'w')

    config = configparser.ConfigParser()
    config.add_section('email')
    # EMAIL USERNAME CONFIGURATION
    config.set('email', 'USERNAME', 'adoption.animaux28@gmail.com')
    # EMAIL PASSWORD CONFIGURATION
    config.set('email', 'PASSWORD', 'Adoption28')
    config.add_section('secret')
    config.set('secret', 'SECRET_KEY', 'Adoption28')
    config.set('secret', 'SECRET_PASSWORD_SALT', 'Adoption28')
    config.add_section('db')
    # DB PATH CONFIGURATION
    config.set('db', 'PATH', 'db/db_tests.db')
    config.write(config_file)
    config_file.close()
